import face_recognition
import skvideo.io
import cv2

# cap = cv2.VideoCapture('test_video.mp4')
#fp = open('face_embed.txt', 'w')
cap = skvideo.io.vreader('test_video.mp4')
all_encodings = []
for frame in cap:
    if frame is None:
        print ('video empty')
        break
    rows,cols = frame.shape[0:2]
    M = cv2.getRotationMatrix2D((cols/2, rows/2),-90,1)
    frame = cv2.warpAffine(frame, M, (cols,rows))
    # get faces
    cv2.imshow('frame', frame)
    face_locations = face_recognition.face_locations(frame)
    # if len(face_locations) == 0:
        # print 'No face found'
    #     continue
    try:
        face_encodings = face_recognition.face_encodings(frame,
                                                         face_locations)[0]
        print ('length of face_encodings {}'.format(len(face_encodings)))
    except:
        print ('No face found in the frame')
    results = face_recognition.compare_faces(all_encodings, face_encodings)
    print (results)
    if not any(results):
        all_encodings.append(face_encodings)
        print 'adding faces'
        cv2.imshow('added face', frame)
    #    break
    cv2.waitKey(1)

print 'number of unique faces = {}'.format(len(all_encodings))
#fp.close()
